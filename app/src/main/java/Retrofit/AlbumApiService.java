package Retrofit;

import Model.Albums;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AlbumApiService {

    @GET("albums")
         Call<Albums>   getAlbums();



}
